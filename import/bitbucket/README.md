Use the official method to import from [Bitbucket Cloud (bitbucket.org)](https://docs.gitlab.com/ee/user/project/import/bitbucket.html)
or [Bitbucket Server (Stash)](https://docs.gitlab.com/ee/user/project/import/bitbucket_server.html).
